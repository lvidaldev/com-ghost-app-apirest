import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegistroComponent } from './components/registro/registro.component';
import { DescargaComponent } from './components/descarga/descarga.component';
import { LoginComponentSegurity } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';


export const ROUTES: Routes=[
    {
        path: 'home', component: HomeComponent
    },

    {
        path: 'registro', component: RegistroComponent
    },

    {
        path: 'descarga', component: DescargaComponent
    },

    { path: 'login', component: LoginComponentSegurity },
    { path: 'register', component: RegisterComponent },
    { path: 'profile', component: ProfileComponent },

    {
        path: '', pathMatch: 'full', redirectTo: 'home'
    },

    {
        path: '**', pathMatch: 'full', redirectTo: 'home'
    }
];