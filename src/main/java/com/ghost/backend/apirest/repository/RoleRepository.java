package com.ghost.backend.apirest.repository;

import java.util.Optional;

import com.ghost.backend.apirest.model.ERole;
import com.ghost.backend.apirest.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
