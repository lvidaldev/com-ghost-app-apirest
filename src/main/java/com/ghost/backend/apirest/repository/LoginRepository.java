package com.ghost.backend.apirest.repository;

import com.ghost.backend.apirest.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRepository extends JpaRepository<Login, String> {
}
