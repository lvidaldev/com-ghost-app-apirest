package com.ghost.backend.apirest.seguridad;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Component;
import com.hiper.hcenter.crypto.DES.TripleDES;

@Component
public class DataSourceComponent {

	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.password}")
	private String password;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.driver-class-name}")
	private String driver_class_name;

	public static String key1 = "1234567890ABCDEF";
	public static String key2 = "ABCDEFGHIJKLMNOP";
	public static String key3 = "QRSTUVWXYZ123456";

	public static DataSourceComponent dataSourceConfiguration;

	public DataSourceComponent() {
	}

	public static DataSource primaryDataSource(DataSourceComponent dataSourceConfiguration) {
		String passDecypher = TripleDES.toString(dataSourceConfiguration.getPassword());
		passDecypher = TripleDES.decipher(passDecypher, key1, key2, key3);
		return DataSourceBuilder.create().url(dataSourceConfiguration.getUrl())
				.username(dataSourceConfiguration.getUsername()).password(passDecypher.trim())
				.driverClassName(dataSourceConfiguration.getDriver_class_name()).build();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDriver_class_name() {
		return driver_class_name;
	}

	public void setDriver_class_name(String driver_class_name) {
		this.driver_class_name = driver_class_name;
	}
}
