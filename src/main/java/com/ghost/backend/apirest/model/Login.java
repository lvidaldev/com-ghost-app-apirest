package com.ghost.backend.apirest.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Login")
public class Login {

    @Id
    @Column(name = "UserID")
    private String userid;
    private Integer AID;
    private String Password;
    @Column(name = "LastConnDate")
    private String lastconndate;
    @Column(name = "LastIP")
    private String lastip;
    private String Passwordz;
    private String Password0;
    private String Code;
    @Column(name="CodeFecha")
    private String codefecha;
    private String Email;

}
