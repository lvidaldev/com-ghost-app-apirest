package com.ghost.backend.apirest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "Account")
public class Account {

    @Id
    @Column(name = "AID")
    private Integer AID;
    private String UserID;
    private Integer UGradeID;
    private Integer PGradeID;
    private String RegDate;
    private Integer VIPWalls;
    private Integer LastVIPWall;
    private String Name;
    private String Email;
    private String RegNum;
    private String Age;
    private String Sex;
    private String ZipCode;
    private String Address;
    private String Country;
    private Integer LastCID;
    private Integer Cert;
    private Integer Question;
    private String Answer;
    private String Status;
    private String EndblockDate;
    private String LastLoginTime;
    private String StriLastLogoutTime;
    private String ServerID;

    /*
    BlockType
            HackingType
    HackingRegTime
            EndHackingBlockTime
    IsPowerLevelingHacker
            PowerLevelingRegDate
    Sa
            Sq
    AvatarURL
            Coins
    ECoins
            ImgURL
    BirthYear
            BirthDay
    BirthMonth
            Tokens
    PostCount
            Signature
    Likes
            Title
    RedColor
            GreenColor
    BlueColor
    */

}
