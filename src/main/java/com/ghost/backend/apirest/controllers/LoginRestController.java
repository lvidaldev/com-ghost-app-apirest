package com.ghost.backend.apirest.controllers;

import com.ghost.backend.apirest.model.Login;
import com.ghost.backend.apirest.service.LoginService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/register")
public class LoginRestController {

    private final LoginService loginService;

    @GetMapping(value = "/findAll")
    public ResponseEntity<List<Login>> findAll() {
        List<Login> responseLogin = loginService.findAll();
        return new ResponseEntity<>(responseLogin, HttpStatus.OK);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<Login> register(@RequestBody Login requestLogin) {
        System.out.println("request enviado 1: " +requestLogin);
        Login responseLogin = loginService.register(requestLogin);
        System.out.println("request enviado 2: " +requestLogin);
        return new ResponseEntity<>(responseLogin, HttpStatus.CREATED);
    }

    /*
    @PostMapping(value = "/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Login create(@RequestBody Login login)
    {
        return loginService.register(login);
    }*/

}
