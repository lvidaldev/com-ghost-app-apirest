package com.ghost.backend.apirest.service.impl;

import com.ghost.backend.apirest.model.Login;
import com.ghost.backend.apirest.repository.LoginRepository;
import com.ghost.backend.apirest.service.LoginService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final LoginRepository loginRepository;

    @Override
    public Login register(Login requestLogin) {
        return loginRepository.save(requestLogin);
    }

    @Override
    public List<Login> findAll() {
        return loginRepository.findAll();
    }

    /*
    @Override
    public TmMerchantChain register(TmMerchantChain requestMerchantChain) {
        return merchantChainRepository.save(requestMerchantChain);
    }

    @Override
    public TmMerchantChain update(TmMerchantChain requestMerchantChain) {
        return merchantChainRepository.save(requestMerchantChain);
    }

    @Override
    public void delete(Integer id) {
        merchantChainRepository.deleteById(id);
    }

    @Override
    public Optional<TmMerchantChain> findById(Integer id) {
        return merchantChainRepository.findById(id);
    }

    @Override
    public List<TmMerchantChain> findAll() {
        return merchantChainRepository.findAll();
    }*/
}
