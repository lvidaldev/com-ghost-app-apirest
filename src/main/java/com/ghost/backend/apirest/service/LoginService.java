package com.ghost.backend.apirest.service;

import com.ghost.backend.apirest.model.Login;

import java.util.List;
import java.util.Optional;

public interface LoginService {

    /*
    TmMerchantChain register(TmMerchantChain requestMerchantChain);

    TmMerchantChain update(TmMerchantChain requestMerchantChain);

    void delete(Integer id);

    Optional<TmMerchantChain> findById(Integer id);*/
    Login register(Login requestLogin);

    List<Login> findAll();
}
